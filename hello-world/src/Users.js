import React, { Component } from 'react';
import logo from './logo.svg';
import './Users.css';

class Users extends Component {
  render() {
    return (
      <div className="Users">
        <p className="Users-intro">
          Static users intro.
        </p>
        <div className="users">
          <table>
          <tr><th>Users</th></tr>
          <tr><td>user1</td></tr>
          <tr><td>user2</td></tr>
          </table>
        </div>
      </div>
    );
  }

}

export default Users;
