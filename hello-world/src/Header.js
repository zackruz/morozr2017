import React, { Component } from 'react';
import Row from './Row';
import './Header.css';

class Header extends Component {

  render() {
    return (
        <thead>
        // Row call in header needs to be able to define between td/th cells usage.
        <tr><th>header row</th></tr>
        </thead>
    );
  }

  renderRow() {
    return <Row />;
  }
}

export default Header;
