import React, { Component } from 'react';
import Users from './Users';
import logo from './logo.svg';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React</h2>
        </div>
        <p className="App-intro">
          Intro.
        </p>
        {this.renderUsers()}
      </div>
    );
  }

  renderUsers() {
    return <Users />;
  }


}

export default App;
