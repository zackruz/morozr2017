import React, { Component } from 'react';
import Row from './Row';
import './Table.css';

class Table extends Component {

  render() {
    return (
      <div className="Table">
      <table>
        <thead>
        <tr><th>header row</th></tr>
        </thead>
      
        <tbody>
        <tr><td>body row</td></tr>
        {this.renderRow()}
        </tbody>
      </table>
      </div>
    );
  }

  renderRow() {
    return <Row />;
  }
}

export default Table;
